#
msgid ""
msgstr ""
"Project-Id-Version: Debian-installer HR\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-07-29 12:23+0200\n"
"PO-Revision-Date: 2021-04-23 18:20+0200\n"
"Last-Translator: Valentin Vidic <vvidic@debian.org>\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:4
msgid "Participate in the package usage survey?"
msgstr "Sudjelovati u anketi korištenja paketa?"

#. Type: boolean
#. Description
#: ../templates:4
msgid ""
"The system may anonymously supply the distribution developers with "
"statistics about the most used packages on this system.  This information "
"influences decisions such as which packages should go on the first "
"distribution CD."
msgstr ""
"Sustav može autorima distribucije anonimno slati statistiku o paketima "
"koji se najviše koriste na ovom računalu. Ti podaci utječu na odluke "
"kao što je izbor paketa koji će se nalaziti na prvom CD-u distribucije."

#. Type: boolean
#. Description
#: ../templates:4
msgid ""
"If you choose to participate, the automatic submission script will run once "
"every week, sending statistics to the distribution developers. The collected "
"statistics can be viewed on https://popcon.debian.org/."
msgstr ""
"Ako odlučite sudjelovati, automatska skripta će jednom tjedno slati "
"statistike autorima distribucije. Sabrane statistike mogu se vidjeti na "
"https://popcon.debian.org/."

#. Type: boolean
#. Description
#: ../templates:4
msgid ""
"This choice can be later modified by running \"dpkg-reconfigure popularity-"
"contest\"."
msgstr ""
"Uvijek se možete predomisliti i promijeniti svoj izbor pokretanjem naredbe: "
"\"dpkg-reconfigure popularity-contest\"."
